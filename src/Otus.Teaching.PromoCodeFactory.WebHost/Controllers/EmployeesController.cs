﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Get employee by id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Add employee
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddEmployeeAsync([FromBody] Employee employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest("Employee is not specified");

            var guid = await _employeeRepository.AddAsync(employeeRequest);

            return Ok($"Employee with id=[{guid}] has been added");
        }

        /// <summary>
        /// Update Employee
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync([FromBody] Employee employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest("Employee for update is not specified");

            var employee = await _employeeRepository.GetByIdAsync(employeeRequest.Id);
            if (employee == null)
                return NotFound("Employee in not found");


            var guid = await _employeeRepository.UpdateAsync(employeeRequest);

            return Ok($"Employee with id=[{guid}] has been updated");
        }

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest("Employee id is not specified");

            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Employee not found");

            var guid = _employeeRepository.DeleteAsync(employee.Id);

            return Ok($"Employee with id=[{guid}] has been deleted");
        }
    }
}